# Sistema de visualización de productos de un Bar
---
## Overview: Problema a resolver
El bar XXX necesita desarrollar un sistema que permita acceder al contenido de los productos, donde el manager del bar debe poder agregar cada uno de los productos, mientras que los empleados podran leer o visualizar los productos sin modificar ninguno de sus valores. Además el manager podra consultar información de cada uno de sus empleados.

### Alcance(Scope)
Descripción..

#### Casos de uso
Descripción...
* Como manager me gustaría poder subir información de un producto
* Como manager me gustaría poder leer información de un producto
* Como manager me gustaría poder modificar información de un producto
* Como manager me gustaría poder leer los datos de un empleado
* Como manager me gustaría poder modificar información de un empleado
* Como empleados me gustaría poder leer información de un producto
* Como empleado me gustaría poder modificar información del mismo empleado
* Como empleado me gustaría poder leer los datos de un empleado
* Como cliente me gustaría poder leer a la información de un producto

#### Out of Scope (casos de uso No Soportados)
Descripción...
* Como empleado me gustaría poder subir información de un producto
* Como empleado me gustaría poder modificar información de otro empleado
* Como cliente me gustaría poder subir información de un producto
* Como cliente me gustaría poder modificar información de un empleado
* Como cliente me gustaría poder modificar información del manager
* Como manager me gustaría poder subir información de un empleado
---
## Arquitectura

### Diagramas

####  Diagrama de Casos de uso

![Casos de Uso](./.readmeStatic/casosdeuso.png)

#### Diagrama de secuencia


#### Diagrama de clase



### Modelo de datos
Poner diseño de entidades, Jsons, tablas, diagramas entidad relación, etc..

---
## Limitaciones
Lista de limitaciones conocidas. Puede ser en formato de lista.
Ej.
* llamada al API que permite subir un producto, no exceda los limites de latencia a 500ms
* Lladamas al API que permite obtener productos para lectura debe de tener una latencia menor a 100ms
---
## Costo
Descripción/Análisis de costos
Ejemplo:
"Considerando N usuarios diarios, M llamadas a X servicio/baseDatos/etc"
* 1000 llamadas diarias a serverless functions. $XX.XX
* 1000 read/write units diarias a X Database on-demand. $XX.XX
Total: $xx.xx (al mes/dia/año)

Contemplando 1000 usuarios diarios, que visitan recurrentemente cada hora